#ifndef MAKI_STRING_H
#define MAKI_STRING_H

extern unsigned long mk_atoi (char *);
extern char*	mk_strcat(char* dest, const char* src);
extern int		mk_strlen(const char* str);
extern char*	mk_getstdin();

#endif