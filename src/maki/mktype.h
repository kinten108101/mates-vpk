#ifndef MAKI_TYPE_H
#define MAKI_TYPE_H

typedef unsigned short uint16;
typedef unsigned short uint16_t;
typedef unsigned char uint8;
typedef unsigned char uint8_t;
typedef unsigned int uint32;
typedef unsigned int uint32_t;
typedef unsigned long long uint64;
typedef unsigned long long uint64_t;

#endif