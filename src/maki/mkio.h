#ifndef MAKI_IOSTREAM_H
#define MAKI_IOSTREAM_H

extern void	mk_ulog (const char *, const char);
extern void	mk_mkdir (const char *);
extern int mkerr (const char * const);
extern void mkdie (const char *);

#endif